package com.example.flipviewlibray;

public enum OverFlipMode {
    GLOW, RUBBER_BAND
}
