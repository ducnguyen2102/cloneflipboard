package com.example.ducng.flipboardsf.common;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ducng.flipboardsf.models.Article;
import com.example.ducng.flipboardsf.models.DummyData;
import com.example.ducng.flipboardsf.models.Link;
import com.example.ducng.flipboardsf.models.Provider;
import com.example.ducng.flipboardsf.models.Topic;
import com.example.ducng.flipboardsf.models.User;

import java.util.List;
import java.util.Objects;

import io.realm.Realm;

public class RealOperation {
    private Realm realm;

    public RealOperation() {
        realm = Realm.getDefaultInstance();
    }

    /**
     * Create dummy data for app
     */

    public void initDummyData(DummyData dummyData) {
        realm.beginTransaction();
        realm.copyToRealm(dummyData.getTopics());
        realm.copyToRealm(dummyData.getProviders());
        realm.copyToRealm(dummyData.getLinks());
        realm.commitTransaction();
    }

    /**
     * Get all topics in Realm
     */

    public List<Topic> getAllTopics() {
        realm.beginTransaction();
        List<Topic> topics = realm.where(Topic.class).findAll();
        realm.commitTransaction();
        return topics;
    }

    /**
     * Get all topic user can add.
     *
     * @param uid user's id
     */

    public List<Topic> getTopicsForAdd(String uid) {
        realm.beginTransaction();
        List<Topic> topics = realm.copyFromRealm(realm.where(Topic.class).findAll());
        User user = realm.copyFromRealm(Objects.requireNonNull(realm.where(User.class).equalTo("uid", uid).findFirst()));
        List<Topic> userTopic = user.getTopics();
        for (Topic topic : userTopic) {
            for (Topic topic1 : topics) {
                if (topic1.getId() == topic.getId()) {
                    topics.remove(topic1);
                    break;
                }
            }
        }
        realm.commitTransaction();
        return topics;
    }

    /**
     * Get all saved articles of user
     *
     * @param uid user's id
     */
    public List<Article> getAllSavedPage(String uid) {
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("uid", uid).findFirst();
        assert user != null;
        List<Article> articles = realm.copyFromRealm(user.getSavedArticles());
        realm.commitTransaction();
        return articles;
    }

    /**
     * Add a user to Realm
     */

    public void addUser(User user) {
        realm.beginTransaction();
        realm.insert(user);
        realm.commitTransaction();
    }

    /**
     * Update a existing user in Realm
     */

    public void updateUser(User user) {
        realm.beginTransaction();
        realm.insertOrUpdate(user);
        realm.commitTransaction();
    }

    /**
     * Get provider name from id
     *
     * @param id provider's id
     */

    public String getProviderName(int id) {
        String result;
        realm.beginTransaction();
        Provider provider = realm.where(Provider.class).equalTo("id", id).findFirst();
        assert provider != null;
        result = provider.getName();
        realm.commitTransaction();
        return result;
    }

    /**
     * Get an existing user in Realm
     */

    public User getUser(String uid) {
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("uid", uid).findFirst();
        realm.commitTransaction();
        return user;
    }

    /**
     * Check an user is exist in Realm
     */

    public boolean isUserExist(String email) {
        realm.beginTransaction();
        boolean isExist = realm.where(User.class).equalTo("email", email).findFirst() != null;
        realm.commitTransaction();
        return isExist;
    }

    /**
     * Show Realm data size to Logcat
     */

    public void logDataSize() {
        Log.e("ducnguyen", "topic size: " + realm.where(Topic.class).findAll().size());
        Log.e("ducnguyen", "provider size: " + realm.where(Provider.class).findAll().size());
        Log.e("ducnguyen", "link size: " + realm.where(Link.class).findAll().size());
        Log.e("ducnguyen", "user size: " + realm.where(User.class).findAll().size());
    }

    /**
     * Get all link of a topic
     */

    public List<Link> getLinkByTopic(Topic topic) {
        realm.beginTransaction();
        List<Link> links = realm.copyFromRealm(realm.where(Link.class).equalTo("topicID", topic.getId()).findAll());
        realm.commitTransaction();
        return links;
    }

    /**
     * Sign up a user by add email to user
     * Used in Email Authenticate
     */

    public void signUpUser(final String uid, final String email) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                User user = realm.where(User.class).equalTo("uid", uid).findFirst();
                assert user != null;
                user.setEmail(email);
            }
        });
    }

    /**
     * Add a topic to user
     */
    public void addTopic(final String uid, final Topic topic) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                User user = realm.where(User.class).equalTo("uid", uid).findFirst();
                Topic topicInRealm = realm.where(Topic.class).equalTo("id", topic.getId()).findFirst();
                assert user != null;
                user.addTopic(topicInRealm);
            }
        });
    }

    /**
     * Save a article for user.
     */
    public void addSavePage(final String uid, final Article article) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                User user = realm.where(User.class).equalTo("uid", uid).findFirst();
                assert user != null;
                user.addSavePage(article);
            }
        });
    }

    /**
     * Check an article is saved by user, isn't it.
     */

    public boolean isSaved(final String uid, final Article article) {
        final boolean[] isSaved = new boolean[1];
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                User user = realm.where(User.class).equalTo("uid", uid).findFirst();
                assert user != null;
                isSaved[0] = user.isSaved(article);
            }
        });
        return isSaved[0];
    }

    /**
     * Sign up a user by add email, display name to user
     * Used in Authenticate with third-party provider
     */

    public void signUpUser(final String uid, final String email, final String displayName) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                User user = realm.where(User.class).equalTo("uid", uid).findFirst();
                assert user != null;
                user.setEmail(email);
                user.setDisplayName(displayName);
            }
        });
    }

    /**
     * Update user's information
     */

    public void addInfor(final String uid, final String displayName, final String username) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                User user = realm.where(User.class).equalTo("uid", uid).findFirst();
                assert user != null;
                user.setDisplayName(displayName);
                user.setUsername(username);
            }
        });
    }
}
