package com.example.ducng.flipboardsf.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.fragments.MainPreferenceFragment;
import com.example.ducng.flipboardsf.interfaces.SettingInterface;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

/**
 * This activity content setting options, started from UserProfileActivity
 */

public class SettingActivity extends AppCompatPreferenceActivity implements SettingInterface{
    private boolean isAnonymousSignIn;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Setting");
        MainPreferenceFragment mainPreferenceFragment = new MainPreferenceFragment();
        mainPreferenceFragment.addOnPrefClickListener(this);
        getFragmentManager().beginTransaction().replace(android.R.id.content, mainPreferenceFragment).commit();
        firebaseAuth = FirebaseAuth.getInstance();
        isAnonymousSignIn = Objects.requireNonNull(firebaseAuth.getCurrentUser()).isAnonymous();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void loginPrefClick() {
        if (isAnonymousSignIn) {
            Intent intent = new Intent(this, AuthActivity.class);
            intent.putExtra("flag", 0);
            startActivity(intent);
        }else {
            GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.google_web_client_id))
                    .requestEmail()
                    .build();
            GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
            googleSignInClient.signOut();
            firebaseAuth.signOut();
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void signUpPrefClick() {
        if (isAnonymousSignIn) {
            Intent intent = new Intent(this, AuthActivity.class);
            intent.putExtra("flag", 1);
            startActivity(intent);
        }
    }
}
