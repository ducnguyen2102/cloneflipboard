package com.example.ducng.flipboardsf.activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ducng.flipboardsf.BaseActivity;
import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.ultilities.Utility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;

import java.util.Objects;

/**
 * This activity for sign in or sign up with email
 * Started from AuthActivity.
 */

public class EmailAuthActivity extends BaseActivity implements View.OnClickListener {
    private TextInputLayout wrapperEmail, wrapperPassword;
    private TextView next, forgot, introTitle, introSubTitle;
    private int flag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_auth);
        flag = getIntent().getIntExtra("flag", 0);
        wrapperEmail = findViewById(R.id.wrapper_email);
        wrapperPassword = findViewById(R.id.wrapper_password);
        next = findViewById(R.id.tv_next);
        next.setOnClickListener(this);
        forgot = findViewById(R.id.forgot_email_password);
        introTitle = findViewById(R.id.intro_title);
        introSubTitle = findViewById(R.id.intro_sub_title);
        if (flag == 1) {
            introTitle.setText(R.string.welcome_to_flipboard);
            introSubTitle.setText(R.string.log_in_or_create_acoount);
            wrapperEmail.setHint(getString(R.string.email));
        }
        Objects.requireNonNull(wrapperEmail.getEditText()).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Utility.validateEmail(s.toString())) {
                    next.setTextColor(Color.WHITE);
                    next.setEnabled(true);
                    wrapperEmail.setError(null);
                } else {
                    next.setTextColor(0xFFD6D6D6);
                    next.setEnabled(false);
                    wrapperEmail.setError("Email Invalid");
                }
            }
        });
        Objects.requireNonNull(wrapperPassword.getEditText()).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Utility.validatePassword(s.toString())) {
                    next.setTextColor(Color.WHITE);
                    next.setEnabled(true);
                    wrapperPassword.setError(null);
                } else {
                    next.setTextColor(0xFFD6D6D6);
                    next.setEnabled(false);
                    wrapperPassword.setError("Password Invalid, length must greater than 6 character");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_next:
                if (wrapperPassword.getVisibility() == View.INVISIBLE) {
                    if (realOperation.isUserExist(Objects.requireNonNull(wrapperEmail.getEditText()).getText().toString())) {
                        flag = 0;
                        introTitle.setText(R.string.log_in);
                        introSubTitle.setText(R.string.to_your_existing_account);
                        wrapperPassword.setVisibility(View.VISIBLE);
                        forgot.setVisibility(View.VISIBLE);
                        next.setEnabled(false);
                        next.setText(getString(R.string.log_in).toUpperCase());
                        next.setTextColor(0xFFD6D6D6);
                        wrapperPassword.requestFocus();
                    } else {
                        if (flag == 0) {
                            wrapperEmail.setError("Email doesn't exist");
                        } else {
                            introTitle.setText(R.string.sign_up);
                            introSubTitle.setText(R.string.for_a_new_account);
                            wrapperPassword.setVisibility(View.VISIBLE);
                            next.setEnabled(false);
                            next.setText(getString(R.string.sign_up).toUpperCase());
                            next.setTextColor(0xFFD6D6D6);
                            wrapperPassword.requestFocus();
                        }
                    }

                } else {
                    hideKeyboard();
                    final String email = Objects.requireNonNull(wrapperEmail.getEditText()).getText().toString();
                    String password = Objects.requireNonNull(wrapperPassword.getEditText()).getText().toString();
                    if (flag == 0) {
                        showProgress();
                        firebaseAuth.signInWithEmailAndPassword(email, password)
                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        hideProgress();
                                        if (task.isSuccessful()) {
                                            uid = firebaseAuth.getUid();
                                            toHomeActivity();
                                        } else {
                                            Toast.makeText(EmailAuthActivity.this, "Sign In Fail!"
                                                    , Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    } else {
                        showProgress();
                        AuthCredential credential = EmailAuthProvider.getCredential(email, password);
                        Objects.requireNonNull(firebaseAuth.getCurrentUser()).linkWithCredential(credential)
                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        hideProgress();
                                        if (task.isSuccessful()) {
                                            realOperation.signUpUser(uid, email);
                                            hideProgress();
                                            toAddInforActivity();
                                        } else {
                                            Toast.makeText(EmailAuthActivity.this, "Sign Up Fail!"
                                                    , Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                }
                break;
        }
    }

    private void toAddInforActivity() {
        Intent intent = new Intent(EmailAuthActivity.this, AddInforActivity.class);
        startActivity(intent);
        finish();
    }
    private void toHomeActivity() {
        Intent intent = new Intent(EmailAuthActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
