package com.example.ducng.flipboardsf.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ducng.flipboardsf.BaseActivity;
import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.models.Article;

import java.util.Objects;

import im.delight.android.webview.AdvancedWebView;

/**
 * This activity for reading content of a article
 */

public class ReadingArticleActivity extends BaseActivity implements AdvancedWebView.Listener {
    private AdvancedWebView webView;
    private ProgressBar progressBar;
    private Article article;
    private boolean isTryed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading_article);
        Toolbar toolbar = findViewById(R.id.toolbar);
        webView = findViewById(R.id.web_view);
        webView.setListener(this, this);
        progressBar = findViewById(R.id.progress_bar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(null);
        }
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        assert bundle != null;
        article = bundle.getParcelable("article");
        assert article != null;
        String link = article.getLink();
        isTryed = false;
        webView.loadUrl(link);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = article.getTitle() + "\nReading now with Flipboard";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Flipboard's hot news");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Sharing With"));
                break;
            case R.id.action_favorite:
                Toast.makeText(this, "Favorite", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_add:
                if (realOperation.isSaved(uid, article)) {
                    Toast.makeText(this, "Already saved!", Toast.LENGTH_SHORT).show();
                } else {
                    if (Objects.requireNonNull(firebaseAuth.getCurrentUser()).isAnonymous()) {
                        showBottomDialog();
                    } else {
                        realOperation.addSavePage(uid, article);
                        Toast.makeText(this, "Saved Article!", Toast.LENGTH_SHORT).show();
                    }

                }
                break;
            case R.id.action_comment:
                Toast.makeText(this, "Comment", Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reading_activity_menu, menu);
        return true;
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        progressBar.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);
    }

    @Override
    public void onPageFinished(String url) {
        progressBar.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
    }

    /**
     * If loading error, try loading page again once.
     * @param errorCode
     * @param description
     * @param failingUrl
     */
    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        if (!isTryed) {
            Toast.makeText(this, "Load article error. Trying again...", Toast.LENGTH_SHORT).show();
            isTryed = true;
        }else {
            Toast.makeText(this,"Can't load this article",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}
