package com.example.ducng.flipboardsf.fragments;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.interfaces.SettingInterface;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

/**
 * Fragment in Setting Activity
 * Show list setting options.
 */

public class MainPreferenceFragment extends PreferenceFragment {
    private SettingInterface settingInterface;
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_main);
        Preference logIn = findPreference(getString(R.string.key_log_in));
        Preference signUp = findPreference(getString(R.string.key_sign_up));
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        boolean isAnonymousAccount = Objects.requireNonNull(firebaseAuth.getCurrentUser()).isAnonymous();
        if (!isAnonymousAccount) {
            logIn.setTitle(getString(R.string.title_log_out));
            signUp.setSummary(null);
            signUp.setTitle(getString(R.string.title_account));
        }
        logIn.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                logInPref();
                return false;
            }
        });
        signUp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                signUpPref();
                return false;
            }
        });
    }
    public void addOnPrefClickListener(SettingInterface settingInterface){
        this.settingInterface = settingInterface;
    }

    private void logInPref() {
        settingInterface.loginPrefClick();
    }

    private void signUpPref() {
        settingInterface.signUpPrefClick();
    }
}

