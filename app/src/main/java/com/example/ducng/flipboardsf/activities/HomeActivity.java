package com.example.ducng.flipboardsf.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.ducng.flipboardsf.BaseActivity;
import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.adapters.HomeTabAdapter;
import com.example.ducng.flipboardsf.fragments.AddTopicFragment;
import com.example.ducng.flipboardsf.fragments.NewsFragment;
import com.example.ducng.flipboardsf.interfaces.AddTopicInterface;
import com.example.ducng.flipboardsf.models.Link;
import com.example.ducng.flipboardsf.models.Topic;
import com.example.ducng.flipboardsf.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This activity is main activity of Application
 * Display articles for each topic
 */
public class HomeActivity extends BaseActivity implements View.OnClickListener, AddTopicInterface {
    private List<Topic> topics, topicsForAdd;
    @SuppressLint("StaticFieldLeak")
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private HomeTabAdapter homeTabAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        updateTopicsData();
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs_layout);
        ImageView userProfile = findViewById(R.id.imv_user_profile);
        userProfile.setOnClickListener(this);
    }

    /**
     * Add a new topic to user account
     */

    private void updateTopicsData() {
        User currentUser = realOperation.getUser(uid);
        topics = currentUser.getTopics();
        topicsForAdd = new ArrayList<>();
        topicsForAdd = realOperation.getTopicsForAdd(uid);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Close Flipboard")
                .setMessage("Are you sure?")
                .setNegativeButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create()
                .show();
    }

    private void setupViewPager(ViewPager viewPager) {
        homeTabAdapter = new HomeTabAdapter(getSupportFragmentManager());
        for (Topic topic : topics) {
            addFragment(topic);
        }
        addTopicFragment();
        viewPager.setAdapter(homeTabAdapter);
        homeTabAdapter.notifyDataSetChanged();
    }

    private void addTopicFragment() {
        AddTopicFragment addTopicFragment = new AddTopicFragment();
        addTopicFragment.addTopicListener(this);
        boolean isSignedIn = !Objects.requireNonNull(firebaseAuth.getCurrentUser()).isAnonymous();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("topics", (ArrayList<? extends Parcelable>) topicsForAdd);
        bundle.putBoolean("isSignedIn", isSignedIn);
        addTopicFragment.setArguments(bundle);
        homeTabAdapter.addFragment(addTopicFragment, "WHAT'S YOUR PASSION?");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_user_profile:
                toUserProfileActivity();
                break;
        }
    }

    private void toUserProfileActivity() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void addTopic(Topic topic) {
        if (!Objects.requireNonNull(firebaseAuth.getCurrentUser()).isAnonymous()) {
            realOperation.addTopic(uid, topic);
            updateTopicsData();
            homeTabAdapter.removeFragment(topics.size()-1);
            addFragment(topic);
            addTopicFragment();
            homeTabAdapter.notifyDataSetChanged();
            viewPager.setCurrentItem(topics.size()-1);
        } else {
            showBottomDialog();
        }
    }

    private void addFragment(Topic topic) {
        NewsFragment newsFragment = new NewsFragment();
        Bundle bundle = new Bundle();
        List<Link> links = realOperation.getLinkByTopic(topic);
        bundle.putParcelableArrayList("links", (ArrayList<? extends Parcelable>) links);
        newsFragment.setArguments(bundle);
        homeTabAdapter.addFragment(newsFragment, topic.getName().toUpperCase());
        homeTabAdapter.notifyDataSetChanged();
    }

}
