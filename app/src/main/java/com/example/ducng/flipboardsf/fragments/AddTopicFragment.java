package com.example.ducng.flipboardsf.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.adapters.AddTopicAdapter;
import com.example.ducng.flipboardsf.interfaces.AddTopicInterface;
import com.example.ducng.flipboardsf.models.Topic;

import java.util.List;

/**
 * Fragment in Home Activity
 * Show list topics can add.
 */

public class AddTopicFragment extends Fragment implements AddTopicInterface{
    private AddTopicInterface addTopicInterface;
    private AddTopicAdapter addTopicAdapter;
    private boolean isSignedIn;
    private List<Topic> topicsForAdd;
    public AddTopicFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public void addTopicListener(AddTopicInterface addTopicInterface){
        this.addTopicInterface = addTopicInterface;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_topic, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.list_topic);
        assert getArguments() != null;
        topicsForAdd = getArguments().getParcelableArrayList("topics");
        isSignedIn = getArguments().getBoolean("isSignedIn",false);
        addTopicAdapter = new AddTopicAdapter(topicsForAdd);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(addTopicAdapter);
        addTopicAdapter.addUpdateListener(this);
        addTopicAdapter.notifyDataSetChanged();
        return view;
    }

    @Override
    public void addTopic(Topic topic) {
        addTopicInterface.addTopic(topic);
        if (isSignedIn){
            topicsForAdd.remove(topic);
            addTopicAdapter.notifyDataSetChanged();
        }
    }

}
