package com.example.ducng.flipboardsf.models;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {
    @PrimaryKey
    private String uid;
    private String email;
    private String username;
    private String displayName;
    private byte[] avatar;
    private RealmList<Topic> topics;
    private RealmList<Provider> providers;
    private RealmList<Article> savedArticles;

    public User() {
    }


    /**
     * Constructor for anonymous user.
     */
    public User(String uid, RealmList<Topic> topics) {
        this.uid = uid;
        this.email = "";
        this.username = "";
        this.displayName = "";
        this.topics = topics;
        this.providers = new RealmList<>();
        this.savedArticles = new RealmList<>();
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public RealmList<Topic> getTopics() {
        return topics;
    }

    public void setTopics(RealmList<Topic> topics) {
        this.topics = topics;
    }

    public RealmList<Provider> getProviders() {
        return providers;
    }

    public void setProviders(RealmList<Provider> providers) {
        this.providers = providers;
    }

    public RealmList<Article> getSavedArticles() {
        return savedArticles;
    }

    public void setSavedArticles(RealmList<Article> savedArticles) {
        this.savedArticles = savedArticles;
    }

    public void addTopic(Topic topic) {
        this.topics.add(topic);
    }

    public void addSavePage(Article article) {
        this.savedArticles.add(article);
    }

    public boolean isSaved(Article article) {
        String link = article.getLink();
        for (Article articleSaved : this.savedArticles) {
            if (link.equals(articleSaved.getLink())) {
                return true;
            }
        }
        return false;
    }
}
