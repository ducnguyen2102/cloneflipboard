package com.example.ducng.flipboardsf.activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.example.ducng.flipboardsf.BaseActivity;
import com.example.ducng.flipboardsf.R;

import java.util.Objects;

/**
 * This activity for adding user information after sign up a new account.
 * User must add Display Name
 * Username is optional
 */

public class AddInforActivity extends BaseActivity {
    private TextInputLayout fullName, userName;
    private TextView next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_infor);
        fullName = findViewById(R.id.wrapper_full_name);
        userName = findViewById(R.id.wrapper_username);
        next = findViewById(R.id.tv_next);
        fullName.setError("Required");
        fullName.setCounterEnabled(true);
        fullName.setCounterMaxLength(50);
        Objects.requireNonNull(fullName.getEditText()).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals("")) {
                    fullName.setError("Required");
                    next.setTextColor(0xFFD6D6D6);
                    next.setEnabled(false);
                } else {
                    fullName.setError("");
                    next.setTextColor(Color.WHITE);
                    next.setEnabled(true);
                }

            }
        });
        Objects.requireNonNull(userName.getEditText()).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((s.toString().equals("")) || (s.toString().length() >= 6)) {
                    userName.setError("");
                    if (Objects.equals(fullName.getError(), "")) {
                        next.setTextColor(Color.WHITE);
                        next.setEnabled(true);
                    } else {
                        next.setTextColor(0xFFD6D6D6);
                        next.setEnabled(false);
                    }
                } else {
                    userName.setError("Length greater than 6 characters");
                    next.setTextColor(0xFFD6D6D6);
                    next.setEnabled(false);
                }
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInfo();
            }
        });
    }

    /**
     * Execute add information to user in Realm
     */
    private void addInfo() {
        String displayName = Objects.requireNonNull(fullName.getEditText()).getText().toString();
        String username = Objects.requireNonNull(userName.getEditText()).getText().toString();
        realOperation.addInfor(uid, displayName, username);
        startActivity(new Intent(this, HomeActivity.class));
    }
}
