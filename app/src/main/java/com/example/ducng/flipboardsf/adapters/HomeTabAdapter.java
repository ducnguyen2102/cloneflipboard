package com.example.ducng.flipboardsf.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;

/**
 * This adapter for set up Tab Layout in Home Activity.
 */

public class HomeTabAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> fragmentList;
    private ArrayList<String> fragmentTitleList;

    public HomeTabAdapter(FragmentManager manager) {
        super(manager);
        fragmentList = new ArrayList<>();
        fragmentTitleList = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        addFragment(fragment,title,fragmentList.size());
    }
    public void addFragment(Fragment fragment, String title, int position){
        fragmentList.add(position,fragment);
        fragmentTitleList.add(position,title);
    }
    public void removeFragment(int position){
        fragmentList.remove(position);
        fragmentTitleList.remove(position);
        notifyDataSetChanged();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}
