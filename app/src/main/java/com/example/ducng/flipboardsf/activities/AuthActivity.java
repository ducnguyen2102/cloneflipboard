package com.example.ducng.flipboardsf.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.ducng.flipboardsf.BaseActivity;
import com.example.ducng.flipboardsf.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Objects;

/**
 * This activity for authenticate.
 * User can sign in or sign up account
 */

public class AuthActivity extends BaseActivity implements View.OnClickListener {
    private GoogleSignInClient googleSignInClient;
    private static final int RC_SIGN_IN = 9001;
    /**
     * Flag=0 when activity start for sign in option
     * Flag=1 when activity start for sign up option
     */
    private int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        RelativeLayout btnLoginEmail = findViewById(R.id.btn_sign_in_email);
        RelativeLayout btnLoginGoogle = findViewById(R.id.btn_sign_in_google);
        RelativeLayout btnLoginFacebook = findViewById(R.id.btn_sign_in_facebook);
        SignInButton googleSignInButton = findViewById(R.id.btn_google_sign_in);
        googleSignInButton.setSize(SignInButton.SIZE_WIDE);
        googleSignInButton.setOnClickListener(this);
        btnLoginEmail.setOnClickListener(this);
        btnLoginGoogle.setOnClickListener(this);
        btnLoginFacebook.setOnClickListener(this);
        flag = getIntent().getIntExtra("flag", 0);
        setUpGoogleSignIn();
    }

    /**
     * Setup Google SignInButton
     */
    private void setUpGoogleSignIn() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_in_email:
                authWithEmail();
                break;
            case R.id.btn_sign_in_google:
                authWithGoogle();
                break;
            case R.id.btn_sign_in_facebook:
                authWithFacebook();
                break;

        }
    }

    /**
     * Authenticate with Facebook Account
     */
    private void authWithFacebook() {

    }

    /**
     * Authenticate with Google Account
     */
    private void authWithGoogle() {
        Intent intent = googleSignInClient.getSignInIntent();
        startActivityForResult(intent, RC_SIGN_IN);
    }

    /**
     * Authenticate with email and password.
     */
    private void authWithEmail() {
        Intent intent = new Intent(this, EmailAuthActivity.class);
        intent.putExtra("flag", flag);
        startActivity(intent);
    }


    /**
     * Process Activity Result
     * if request code = Google Sign In code:
     * get account from activity result, after sign in Google.
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                processGoogleAccount(account);

            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Process Google Account after sign in from Google
     * if account's email exists in Realm, just sign in to Firebase
     * else, if action is sign in, show Toast account doesn't exists
     * if action is sign up, update anonymous account with information in account
     * and convert anonymous account permanent account.
     */

    private void processGoogleAccount(GoogleSignInAccount account) {
        if (realOperation.isUserExist(account.getEmail())) {
            signInWithGoogleAccount(account);
        } else {
            if (flag == 0) {
                googleSignInClient.signOut();
                Toast.makeText(this, "Account doesn't exist!", Toast.LENGTH_SHORT).show();
            } else {
                signUpWithGoogleAccount(account);
            }
        }

    }

    /**
     * Sign in with Google account via Firebase Authenticate
     *
     * @param account is account received from Google
     */

    private void signInWithGoogleAccount(GoogleSignInAccount account) {
        showProgress();
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgress();
                        if (task.isSuccessful()) {
                            uid = firebaseAuth.getUid();
                            toHomeActivity();
                        } else {
                            Toast.makeText(AuthActivity.this, "Sign in fail!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /**
     * Add user data to Realm.
     */

    private void signUpWithGoogleAccount(GoogleSignInAccount account) {
        String email = account.getEmail();
        String displayName = account.getDisplayName();
        realOperation.signUpUser(uid, email, displayName);
        linkWithGoogleAccount(account);
    }

    /**
     * Sign up via Google account with Firebase Authenticate
     *
     * @param account is account received from Google
     */
    private void linkWithGoogleAccount(GoogleSignInAccount account) {
        showProgress();
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        Objects.requireNonNull(firebaseAuth.getCurrentUser()).linkWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgress();
                        if (task.isSuccessful()) {
                            uid = firebaseAuth.getUid();
                            toHomeActivity();
                        } else {
                            Toast.makeText(AuthActivity.this, "Sign in fail!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    /**
     * If sign in or sign up success, run this method to HomeActivity
     */
    private void toHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }


}