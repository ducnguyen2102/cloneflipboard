package com.example.ducng.flipboardsf;

import android.app.Application;

import com.google.gson.Gson;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class App extends Application {
    private static App app;
    private static Gson gson;
    public static App getSelf() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        gson = new Gson();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
    }
    public Gson getGson(){
        return gson;
    }

}
