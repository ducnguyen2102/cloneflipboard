package com.example.ducng.flipboardsf.interfaces;

import com.example.ducng.flipboardsf.models.Topic;

public interface AddTopicInterface {
    void addTopic(Topic topic);
}
