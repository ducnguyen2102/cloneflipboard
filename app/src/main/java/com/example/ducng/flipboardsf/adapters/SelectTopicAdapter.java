package com.example.ducng.flipboardsf.adapters;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.interfaces.SelectTopicsInterface;
import com.example.ducng.flipboardsf.models.Topic;

import java.util.List;

/**
 * This adapter for list topics user can choose to add.
 */

public class SelectTopicAdapter extends RecyclerView.Adapter<SelectTopicAdapter.ViewHolder> {
    private List<Topic> topics;
    private boolean[] chooseData;
    private SelectTopicsInterface selectTopicsInterface;

    public SelectTopicAdapter(List<Topic> topics) {
        this.topics = topics;
        chooseData = new boolean[topics.size()];
    }

    public void addUpdateListener(SelectTopicsInterface selectTopicsInterface) {
        this.selectTopicsInterface = selectTopicsInterface;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        RelativeLayout relativeLayout;
        boolean isChoose;

        ViewHolder(View topicView) {
            super(topicView);
            textView = topicView.findViewById(R.id.name);
            relativeLayout = topicView.findViewById(R.id.item_view);
            isChoose = false;
        }
    }


    @NonNull
    @Override
    public SelectTopicAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.textView.setText("#" + topics.get(position).getName().toUpperCase());
        if (!chooseData[position]) {
            holder.textView.setBackgroundColor(Color.parseColor("#efefef"));
            holder.textView.setTextColor(Color.parseColor("#9c9c9c"));
        } else {
            holder.textView.setBackgroundColor(Color.parseColor("#f52828"));
            holder.textView.setTextColor(Color.parseColor("#ffffff"));
        }
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseData[position] = !chooseData[position];
                selectTopicsInterface.selectTopic(position);
                notifyItemChanged(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return topics.size();
    }
}
