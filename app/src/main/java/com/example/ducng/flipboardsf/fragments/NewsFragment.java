package com.example.ducng.flipboardsf.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.adapters.ArticleAdapter;
import com.example.ducng.flipboardsf.common.ParserTask;
import com.example.ducng.flipboardsf.common.RealOperation;
import com.example.ducng.flipboardsf.models.Article;
import com.example.ducng.flipboardsf.models.Link;
import com.example.flipviewlibray.FlipView;

import java.util.List;

/**
 * Fragment in Home Activity
 * Show list articles
 */
public class NewsFragment extends Fragment {
    private ProgressBar progressBar;
    private FlipView flipView;
    private ArticleAdapter articleAdapter;
    private int indexLoadedLink;
    private List<Link> links;
    private RealOperation realOperation;
    private boolean isLoading;

    public NewsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        progressBar = view.findViewById(R.id.progress_bar);
        flipView = view.findViewById(R.id.flipview);
        assert getArguments() != null;
        links = getArguments().getParcelableArrayList("links");
        articleAdapter = new ArticleAdapter(getContext());
        flipView.setAdapter(articleAdapter);
        realOperation = new RealOperation();
        assert links != null;
        indexLoadedLink = 0;
        if (indexLoadedLink<links.size()-1){
            loadLink();
        }
        flipView.setOnFlipListener(new FlipView.OnFlipListener() {
            @Override
            public void onFlippedToPage(FlipView v, int position, long id) {
                if (flipView.getPageCount() - position < 10 && !isLoading) {
                    loadLink();

                }
            }
        });
        return view;
    }

    private void loadLink() {
        Link currentLink = links.get(indexLoadedLink);
        String providerName = realOperation.getProviderName(currentLink.getProviderID());
        loadRSS(currentLink, providerName);
        isLoading = true;
        indexLoadedLink++;
    }


    @SuppressLint("StaticFieldLeak")
    private void loadRSS(final Link link, String name) {
        ParserTask parserTask = new ParserTask(name);
        parserTask.execute(link.getLinkURL());
        parserTask.onFinish(new ParserTask.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(List<Article> list) {
                progressBar.setVisibility(View.GONE);
                if (flipView.getVisibility() == View.GONE) {
                    flipView.setVisibility(View.VISIBLE);
                }
                articleAdapter.addArticle(list);
                isLoading = false;
            }

            @Override
            public void onError() {
                isLoading = false;
                Log.e("ducnguyen", "loadRSS error");
            }
        });
    }
}
