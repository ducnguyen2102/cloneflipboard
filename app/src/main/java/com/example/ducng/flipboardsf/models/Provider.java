package com.example.ducng.flipboardsf.models;


import io.realm.RealmObject;

public class Provider extends RealmObject {
    private int id;
    private String name;
    private String homeURL;

    public Provider() {
    }

    public Provider(int id, String name, String homeURL) {
        this.id = id;
        this.name = name;
        this.homeURL = homeURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHomeURL() {
        return homeURL;
    }

    public void setHomeURL(String homeURL) {
        this.homeURL = homeURL;
    }
}
