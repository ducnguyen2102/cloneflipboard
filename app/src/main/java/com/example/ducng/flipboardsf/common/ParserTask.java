package com.example.ducng.flipboardsf.common;

import android.os.AsyncTask;

import com.example.ducng.flipboardsf.models.Article;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Parse a url to list articles
 */
public class ParserTask extends AsyncTask<String, Void, String> implements Observer {

    private XMLParser xmlParser;
    private List<Article> articles;
    private String providerName;
    private OnTaskCompleted onComplete;

    public ParserTask(String providerName) {
        this.articles = new ArrayList<>();
        xmlParser = new XMLParser(this.articles);
        xmlParser.addObserver(this);
        this.providerName = providerName;
    }

    public interface OnTaskCompleted {
        void onTaskCompleted(List<Article> list);

        void onError();
    }

    public void onFinish(OnTaskCompleted onComplete) {
        this.onComplete = onComplete;
    }

    @Override
    protected String doInBackground(String... ulr) {

        Response response;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(ulr[0])
                .build();
        try {
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return Objects.requireNonNull(response.body()).string();
            }
        } catch (IOException e) {
            e.printStackTrace();
            onComplete.onError();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {

        if (result != null) {
            try {
                xmlParser.parseXML(result,providerName);
            } catch (Exception e) {
                e.printStackTrace();
                onComplete.onError();
            }
        } else
            onComplete.onError();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Observable observable, Object data) {
        articles = (List<Article>) data;
        onComplete.onTaskCompleted(articles);
    }

}
