package com.example.ducng.flipboardsf.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.models.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * This adapter for show all saved pages of user.
 */

public class SavedPageAdapter extends RecyclerView.Adapter<SavedPageAdapter.ViewHolder>{
    private List<Article> articles;
    private Context context;

    public SavedPageAdapter(List<Article> articles, Context context) {
        this.articles = articles;
        this.context = context;
    }
    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView itemSavedTitle;
        ImageView itemSavedCover;
        ViewHolder(View v){
            super(v);
            itemSavedTitle = v.findViewById(R.id.item_saved_tile);
            itemSavedCover = v.findViewById(R.id.item_saved_cover);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_saved_article,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemSavedTitle.setText(articles.get(position).getTitle());
        Picasso.get().load(articles.get(position).getImage())
                .error(R.drawable.image_cover)
                .placeholder(R.drawable.loading_image)
                .into(holder.itemSavedCover);

    }

    @Override
    public int getItemCount() {
        return articles.size();
    }
}
