package com.example.ducng.flipboardsf.activities;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.example.ducng.flipboardsf.BaseActivity;
import com.example.ducng.flipboardsf.common.Storage;
import com.example.ducng.flipboardsf.models.DummyData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This activity show when app launch
 * If user is exist, move to HomeActivity
 * If user isn't exist, move to WelcomeActivity.
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Storage.getInstance().isNotFirstTimeRun()) {
            initDummyData();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (firebaseAuth.getCurrentUser() == null) {
                    intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                } else {
                    uid = FirebaseAuth.getInstance().getUid();
                    intent = new Intent(SplashActivity.this, HomeActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, 1000);
    }

    /**
     * If first time run of application, init dummy data.
     * Dummy data includes topics, links and providers
     */

    private void initDummyData() {
        BufferedReader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        String jsonStringData;
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("dummydata.json")));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException ignored) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {
                }
            }
        }
        jsonStringData = stringBuilder.toString();
        DummyData dummyData = new Gson().fromJson(jsonStringData, DummyData.class);
        realOperation.initDummyData(dummyData);
        Storage.getInstance().setNotFirstTimeRun(true);
    }
}
