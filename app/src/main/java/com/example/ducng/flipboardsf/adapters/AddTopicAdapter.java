package com.example.ducng.flipboardsf.adapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.interfaces.AddTopicInterface;
import com.example.ducng.flipboardsf.models.Topic;
import com.example.ducng.flipboardsf.ultilities.Utility;

import java.util.List;

/**
 * This adapter for show list topics, which user can add to account
 */

public class AddTopicAdapter extends RecyclerView.Adapter<AddTopicAdapter.ViewHolder> {
    private List<Topic> topics;
    private AddTopicInterface addTopicInterface;

    public AddTopicAdapter(List<Topic> topics) {
        this.topics = topics;
    }

    public void addUpdateListener(AddTopicInterface addTopicInterface) {
        this.addTopicInterface = addTopicInterface;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView topicTitle;
        TextView numberReader;

        ViewHolder(View topicView) {
            super(topicView);
            topicTitle = topicView.findViewById(R.id.title_topic);
            numberReader = topicView.findViewById(R.id.num_reader);
        }
    }


    @NonNull
    @Override
    public AddTopicAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_topic, parent, false);
        return new AddTopicAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.topicTitle.setText(topics.get(position).getName().toUpperCase());
        holder.numberReader.setText(Utility.getRandomNumberOfReader());
        holder.topicTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTopicInterface.addTopic(topics.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return topics.size();
    }
}
