package com.example.ducng.flipboardsf.models;


import java.util.List;

/**
 * Object for Dummy Data. Created from a JSON file.
 */
public class DummyData {


    private List<Provider> providers;

    private List<Topic> topics;

    private List<Link> links;

    public DummyData(List<Provider> providers, List<Topic> topics, List<Link> links) {
        this.providers = providers;
        this.topics = topics;
        this.links = links;
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
