package com.example.ducng.flipboardsf;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


import com.example.ducng.flipboardsf.activities.AuthActivity;
import com.example.ducng.flipboardsf.common.RealOperation;
import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;


public class BaseActivity extends AppCompatActivity {
    public RealOperation realOperation;
    public FirebaseAuth firebaseAuth;
    public static String uid;
    private ProgressDialog progressDialog;
    private boolean isDetachedFromWindow = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realOperation = new RealOperation();
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    public synchronized void showProgress(String message) {
        if (!TextUtils.isEmpty(message)) {
            progressDialog.setMessage(message);
        }


        if (!progressDialog.isShowing() && !isFinishing()) {
            progressDialog.show();
        }
    }

    public synchronized void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing() && !isDetachedFromWindow) {
            progressDialog.dismiss();
        }
    }

    public void showProgress() {
        showProgress("Loading...");
    }

    @Override
    public void onDetachedFromWindow() {
        isDetachedFromWindow = true;
        super.onDetachedFromWindow();
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE))).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void showBottomDialog() {
        BottomDialog bottomDialog = new BottomDialog.Builder(this)
                .setTitle("Create an account to continue")
                .setContent("To make personalized Smart Magazines, you'll need a Flipboard account. We'll save your previous content.")
                .setNegativeText("Not now")
                .setPositiveText("Continue")
                .setNegativeTextColorResource(R.color.colorAccent)
                .setPositiveTextColorResource(R.color.colorPrimary)
                .setPositiveBackgroundColor(0xf52828)
                .setCancelable(false)
                .setPositiveBackgroundColorResource(R.color.colorAccent)
                .onNegative(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull BottomDialog bottomDialog) {
                        bottomDialog.dismiss();
                    }
                })
                .onPositive(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull BottomDialog bottomDialog) {
                        Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
                        intent.putExtra("flag", 1);
                        startActivity(intent);
                    }
                })
                .build();
        bottomDialog.show();
    }
}

