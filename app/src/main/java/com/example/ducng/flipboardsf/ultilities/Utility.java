package com.example.ducng.flipboardsf.ultilities;


import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utilities for Application.
 */

public class Utility {
    private static final String EMAIL_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    /**
     * Check an email is validated, isn't it?
     */
    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher;
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Check an password is validated, isn't it?
     */

    public static boolean validatePassword(String password) {
        return password.length() >= 6;
    }

    /**
     * Get time from date input to current time.
     */
    public static String howLongFrom(Date fromTime) {
        String result;
        long time = (Calendar.getInstance().getTime().getTime() - fromTime.getTime()) / 1000;
        if (time / 60 < 60) {
            result = (time / 60) + "m ago";
        } else if (time / 3600 < 24) {
            result = (time / 3600) + "h ago";
        } else {
            result = (time / 86400) + "d ago";
        }
        return result;
    }

    public static String getRandomNumberOfReader() {
        Random random = new Random();
        int value = random.nextInt(10) + 1;
        return value + "M reading about this";
    }
}
