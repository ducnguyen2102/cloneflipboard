package com.example.ducng.flipboardsf.common;


public class Storage {
    private static Storage storage;
    private boolean notFirstTimeRun;
    private static final String KEY_TIME_RUN = "time_run";

    public static Storage getInstance() {
        if (storage == null) {
            storage = new Storage();
        }
        return storage;
    }

    public boolean isNotFirstTimeRun() {
        notFirstTimeRun = SharedPrefs.getInstance().get(KEY_TIME_RUN, Boolean.class);
        return notFirstTimeRun;
    }

    public void setNotFirstTimeRun(boolean notFirstTimeRun) {
        this.notFirstTimeRun = notFirstTimeRun;
        SharedPrefs.getInstance().put(KEY_TIME_RUN, notFirstTimeRun);
    }
}

