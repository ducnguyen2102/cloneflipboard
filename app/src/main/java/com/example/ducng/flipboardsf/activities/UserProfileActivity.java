package com.example.ducng.flipboardsf.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ducng.flipboardsf.BaseActivity;
import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.adapters.SavedPageAdapter;
import com.example.ducng.flipboardsf.common.RecyclerItemClickListener;
import com.example.ducng.flipboardsf.models.Article;
import com.example.ducng.flipboardsf.models.User;

import java.util.List;
import java.util.Objects;

/**
 * This activity shows user's information and show list saved articles of user
 */

public class UserProfileActivity extends BaseActivity implements View.OnClickListener {
    private User currentUser;
    private List<Article> articles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        TextView signUp = findViewById(R.id.sign_up);
        signUp.setOnClickListener(this);
        ImageView setting = findViewById(R.id.imv_setting);
        setting.setOnClickListener(this);
        ImageView follow = findViewById(R.id.imv_add_follow);
        follow.setOnClickListener(this);
        currentUser = realOperation.getUser(uid);
        if (!Objects.requireNonNull(firebaseAuth.getCurrentUser()).isAnonymous()) {
            showSignInUI();
        } else {
            showNotSignInUI();
        }
    }

    @SuppressLint("SetTextI18n")
    private void showSignInUI() {
        TextView displayName = findViewById(R.id.tv_user_display_name);
        TextView username = findViewById(R.id.tv_user_name);
        TextView savedPage = findViewById(R.id.tv_saved_page);
        View signUpView = findViewById(R.id.view_sign_up);
        signUpView.setVisibility(View.GONE);
        displayName.setText(currentUser.getDisplayName());
        username.setVisibility(View.VISIBLE);
        username.setText(currentUser.getUsername().equals("") ? "" : "@" + currentUser.getUsername());
        articles = realOperation.getAllSavedPage(uid);
        savedPage.setText(articles.size() + " Page" + (articles.size() > 1 ? "s" : "") + " Saved");
        RecyclerView recyclerView = findViewById(R.id.save_articles);
        RecyclerView.LayoutManager recyclerViewLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        SavedPageAdapter adapter = new SavedPageAdapter(articles, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(UserProfileActivity.this, ReadingArticleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("article", articles.get(position));
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View view, int position) {
                Toast.makeText(UserProfileActivity.this, "Long clicked!", Toast.LENGTH_SHORT).show();
            }
        }));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up:
                toSignUpActivity();
                break;
            case R.id.imv_setting:
                toSettingActivity();
                break;
            case R.id.imv_add_follow:
                break;
        }
    }

    private void toSettingActivity() {
        startActivity(new Intent(this, SettingActivity.class));
    }

    private void toSignUpActivity() {
        Intent intent = new Intent(this, AuthActivity.class);
        intent.putExtra("flag", 1);
        startActivity(intent);
    }

    private void showNotSignInUI() {

    }
}
