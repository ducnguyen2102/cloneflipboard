package com.example.ducng.flipboardsf.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.activities.ReadingArticleActivity;
import com.example.ducng.flipboardsf.models.Article;
import com.example.ducng.flipboardsf.ultilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This adapter for show articles in Tab layout in Home Activity
 */

public class ArticleAdapter extends BaseAdapter {
    private List<Article> articles;
    private Context context;

    public ArticleAdapter(Context context) {
        this.articles = new ArrayList<>();
        this.context = context;
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Object getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_article_test, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Article currentArticle = articles.get(position);
        viewHolder.tvTitle.setText(currentArticle.getTitle().trim());
        viewHolder.tvDescription.setText(currentArticle.getDescription().trim());
        Picasso.get().load(currentArticle.getImage())
                .error(R.drawable.image_cover)
                .placeholder(R.drawable.loading_image)
                .into(viewHolder.imvCover);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReadingArticleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("article",currentArticle);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
        String timeStamp="";
        try {
            Date date = new Date(currentArticle.getPubDate());
            timeStamp = Utility.howLongFrom(date);
        }catch (Exception e){
            e.printStackTrace();
        }
        viewHolder.tvTimeStamp.setText(timeStamp);
        viewHolder.provider.setText(currentArticle.getProviderName());
        return convertView;
    }
    public void addArticle(List<Article> articles){
        this.articles.addAll(articles);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView tvTitle, tvDescription,tvTimeStamp,provider;
        ImageView imvCover;

        ViewHolder(View view) {
            tvTitle = view.findViewById(R.id.tv_title);
            tvDescription = view.findViewById(R.id.tv_description);
            tvTimeStamp = view.findViewById(R.id.time_stamp);
            provider = view.findViewById(R.id.provider);
            imvCover = view.findViewById(R.id.imv_cover);
        }
    }
}
