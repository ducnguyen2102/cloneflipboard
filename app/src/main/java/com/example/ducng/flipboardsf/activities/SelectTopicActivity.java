package com.example.ducng.flipboardsf.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.ducng.flipboardsf.BaseActivity;
import com.example.ducng.flipboardsf.R;
import com.example.ducng.flipboardsf.adapters.SelectTopicAdapter;
import com.example.ducng.flipboardsf.interfaces.SelectTopicsInterface;
import com.example.ducng.flipboardsf.models.Topic;
import com.example.ducng.flipboardsf.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import java.util.List;

import io.realm.RealmList;

/**
 * This topic for user chooses which topics to show
 * User must choose greater than 3 topics.
 */

public class SelectTopicActivity extends BaseActivity implements View.OnClickListener, SelectTopicsInterface, OnCompleteListener<AuthResult> {
    private TextView chooseTopic;
    private List<Topic> topics;
    private boolean[] chooseData;
    private Drawable BACK_GROUND_RED;
    private Drawable BACK_GROUND_GRAY;
    private int numberTopicsSelected = 0;
    private RealmList<Topic> topicsSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_topic);
        topics = realOperation.getAllTopics();
        chooseData = new boolean[topics.size()];
        chooseTopic = findViewById(R.id.choose_and_continue);
        TextView logIn = findViewById(R.id.log_in);
        RecyclerView recyclerView = findViewById(R.id.list_topic);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        chooseTopic.setOnClickListener(this);
        logIn.setOnClickListener(this);
        SelectTopicAdapter adapter = new SelectTopicAdapter(topics);
        adapter.addUpdateListener(this);
        recyclerView.setAdapter(adapter);
        BACK_GROUND_RED = getDrawable(R.drawable.background_textview_red);
        BACK_GROUND_GRAY = getDrawable(R.drawable.background_textview_gray);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.log_in:
                toHomeActivity();
                break;
            case R.id.choose_and_continue:
                if (numberTopicsSelected >= 3) {
                    selectTopics();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Get list topic selected
     */

    private void selectTopics() {
        showProgress();
        topicsSelected = new RealmList<>();
        for (int index = 0; index < chooseData.length; index++) {
            if (chooseData[index]) {
                topicsSelected.add(topics.get(index));
            }
        }
        firebaseAuth.signInAnonymously().addOnCompleteListener(this);
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        if (task.isSuccessful()) {
            User user = new User(firebaseAuth.getUid(), topicsSelected);
            realOperation.addUser(user);
            uid = firebaseAuth.getUid();
            Intent intent = new Intent(this, HomeActivity.class);
            hideProgress();
            startActivity(intent);
            finish();
        }
    }

    /**
     * After people chooses topics done, move to MainActivity
     */
    private void toHomeActivity() {
        Intent intent = new Intent(this, AuthActivity.class);
        intent.putExtra("flag",0);
        startActivity(intent);
    }

    /**
     * Set mark for selected topics.
     * @param position
     */
    @SuppressLint("SetTextI18n")
    @Override
    public void selectTopic(int position) {
        chooseData[position] = !chooseData[position];
        if (chooseData[position]) {
            numberTopicsSelected++;
        } else {
            numberTopicsSelected--;
        }
        if (numberTopicsSelected >= 3) {
            chooseTopic.setBackground(BACK_GROUND_RED);
            chooseTopic.setText("Continue");
        } else {
            chooseTopic.setBackground(BACK_GROUND_GRAY);
            chooseTopic.setText("Follow 3 or more Topics");
        }
    }
}
