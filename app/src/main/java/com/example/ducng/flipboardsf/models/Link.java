package com.example.ducng.flipboardsf.models;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

public class Link extends RealmObject implements Parcelable{
    private int id;
    private int topicID;
    private int providerID;
    private String linkURL;

    public Link() {
    }

    public Link(int id, int topicID, int providerID, String linkURL) {
        this.id = id;
        this.topicID = topicID;
        this.providerID = providerID;
        this.linkURL = linkURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTopicID() {
        return topicID;
    }

    public void setTopicID(int topicID) {
        this.topicID = topicID;
    }

    public int getProviderID() {
        return providerID;
    }

    public void setProviderID(int providerID) {
        this.providerID = providerID;
    }

    public String getLinkURL() {
        return linkURL;
    }

    public void setLinkURL(String linkURL) {
        this.linkURL = linkURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(topicID);
        dest.writeInt(providerID);
        dest.writeString(linkURL);
    }
    public static final Parcelable.Creator<Link> CREATOR = new Parcelable.Creator<Link>() {
        public Link createFromParcel(Parcel source) {
            return new Link(source);
        }

        @Override
        public Link[] newArray(int size) {
            return new Link[size];
        }
    };

    private Link(Parcel in) {
        id = in.readInt();
        topicID = in.readInt();
        providerID = in.readInt();
        linkURL = in.readString();
    }
}
